SYSTEM_MODE(MANUAL);

void setup ()
  {
    Serial.begin(9600);
    pinMode(D7, OUTPUT);
  }

void loop ()
  {
    Serial.println("connect()");
    connect();
    Particle.process();

    Serial.println("LED On");
    delay(5000);
    Particle.process();
    
    Particle.publish("led", "on", 60, PRIVATE);
    Particle.process();

    digitalWrite(D7, HIGH);
    Particle.process();

    delay(1000);
    Serial.println("LED Off");
    Particle.publish("led", "off", 60, PRIVATE);
    Particle.process();

    digitalWrite(D7, LOW);
    delay(1000);
    Particle.process();

    Serial.println("WiFi.off()");
    WiFi.off();
    delay(10000);
    Particle.process();
  }

void connect() {
  WiFi.on();
  Particle.process();
  WiFi.connect();
  Particle.process();

  if (WiFi.ready() == false)
    {
      delay(100);
      Particle.process();
    }
  if (Particle.connected() == false)
    {
    Particle.connect();
    Particle.process();
    delay(100);
    }
  }
